
from typing import DefaultDict
from odoo import models, fields, api

class mi_adiestramiento(models.Model):
     _name = 'mi_adiestramiento.mi_adiestramiento.familiares'
     _description="este es un modelo para los familiares"


     nombre = fields.Char(string='Nombre',)
     parentesco = fields.Selection([('hijo', 'hijo'),('esposo', 'esposo'),('esposa', 'esposa')])
     edad = fields.Integer('edad')
     
     contacto_id= fields.Many2one('mi_adiestramiento.mi_adiestramiento','contacto_id')