# -*- coding: utf-8 -*-

from typing import DefaultDict
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class mi_adiestramiento(models.Model):
     # nombre del modelo
     _name = 'mi_adiestramiento.mi_adiestramiento'
     # descripcion del modelo
     _description="este es un modelo de prueba"
     # herencia de otro modelo
     _inherit = ['mail.thread']
     # condiciones para un modelo
     _sql_constraints = [('documento_unico', 'unique(documento,activo)',("El documento debe ser único"))]
     # campo texto
     name = fields.Char(string='nombre',required=True,help='ayuda al usuario',default='jose')
     # campo booleano
     activo = fields.Boolean('activo?',default=True)
     # campo seleccion de un diccionario
     state=fields.Selection(
          [('borrador',u"borrador"),
          ('validado',u"validado"),
          ('cancelado',u"cancelado")],
          string='state',required=True,default='borrador' , track_visibility='onchange')
     
     documento = fields.Char(string='documento')
     
     # campo entero
     edad = fields.Integer('Edad' )
     
     # campo fecha 
     nacimiento = fields.Date('fecha de nacimiento')
     
     # campo fecha con hora
     hora = fields.Datetime('hora')
     
     
     product_id = fields.Many2one('product.template',string='producto fav')
     # campo float relacionado a otro modelo
     precio_producto = fields.Float(related="product_id.standard_price",string ="Precio Producto")
     
     # campo flotante
     credito = fields.Float('credito')
     
     foto = fields.Binary(string='foto')
     
     # campos relacionales
     currency_id= fields.Many2one('res.currency',string='Moneda')
     # campo relacional a otro moledo
     familiares = fields.One2many('mi_adiestramiento.mi_adiestramiento.familiares','contacto_id')
     
     productos_favoritos_ids = fields.Many2many('product.template')
     
     # campo computarizado
     cantidad_familiares = fields.Integer('No. Familiares', compute='contar_familiares')
     edad_total = fields.Integer('Edad Total', compute='contar_edad')
     
     # campo texto para un documento y campo documento
     archivo_name = fields.Char('Nombre del archivo')
     archivo = fields.Binary('Mi archivo')
     
     sale_order_count = fields.Float('sale_order_count', compute='_sale_order_count')
     
     
     def validar(self):
          for rec in self:
               rec.state = 'validado'
               
     def uerror(self):
          for rec in self:
               raise UserError(_('Only Leads can approve leave requests.'))
          
          
          
     @api.depends('familiares')
     def contar_familiares(self):
          for rec in self:
               cantidad = len(rec.familiares)
               rec.cantidad_familiares = cantidad
     
     
     @api.depends('familiares')
     def contar_edad(self):
          for rec in self:
               edad_total=0
               for integrante in rec.familiares:
                    edad_total= edad_total + integrante.edad
               rec.edad_total=edad_total
                   
     @api.depends('sale_order_count')
     def _sale_order_count(self):
          for rec in self:
               monto_total=0
               # env para invocar otro modelo y search si no se pasa parametro retorna todo
               productos_id=self.env['product.template'].search([])
               for p in productos_id:
                    monto_total=monto_total + p.standard_price
               rec.sale_order_count=monto_total
     