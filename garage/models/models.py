# -*- coding: utf-8 -*-

from odoo import models, fields, api


class aparcamiento(models.Model):
    _name = 'garage.aparcamiento'
    _description = 'descripcion apacamiento '


    name = fields.Char('Direccion',required=True,)
    plazas= fields.Integer('plazas', required=True,)


class coche(models.Model):
    _name = 'garage.coche'
    _description = 'descripcion del coche'
 	_order='name desc'

    name = fields.Char('matricula',required=True, size=7 )
    modelo= fields.Char('modelo',required=True,)
    construido=fields.Date(string='fecha de construido')
    averiado = fields.Boolean('Averiado',default=False)
    consumo= fields.Float('consumo',(4,1),default='0.0',help='consumo promedio por cada 100km' )
    annos= fields.Integer('annos',compute='_get_annos')
    descripcion = fields.Text('descripcion')

    @api.depends('construido')
    def _get_annos(self):
        for coche in self:
    	   	coche.annos=0

class mantenimiento(models.Model):
    _name = 'garage.mantenimiento'
    _description = 'permite definir mantenimientosobre conjunto de coche'
#    _order = 'Fecha'

    fecha = fields.Date('fecha', required=True, default=fields.Date.today())
    tipo=fields.Selection('tipo',Selection=[('l','lavar'),('r','revision'),('m','mecanica'),('p','pintura')])
    coste = fields.Float('coste',(8,2),help="coste total de mantenimiento")
  	    