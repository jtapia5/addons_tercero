# -*- coding: utf-8 -*-
{
    'name': "adiestramiento",

    'summary': """
        modulo de prueba""",

    'description': """
        Modulo de prueba creado para la capacitacion
    """,

    'author': "Lost Mind",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/10.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','mail','crm','sale'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/mi_adiestramiento.xml',
        'views/res_partner.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}